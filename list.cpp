#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if (tail == 0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)//add list at tail
{
	Node *tmp = new Node(el, 0); //Create new node that store character and pointer to null
	if (tail == 0 ) //If there is nothing in the list
	{
		tail = tmp; //tail point to tmp
		head = tmp; //head point same as tail
	}
	else //If there are some node in the list
	{
		tail->next = tmp; //linked the tail to tmp
		tail = tmp; //make tail point at tmp
	}
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()//Delete tail
{
	char el = tail->data; //Store tail data before delete tail
	Node *nowNode = head; //Create node point to head
	Node *atTail = tail; //Create node point to tail
	if (head == tail)  //If head and tail point at same locate
	{
		head = tail = 0; //Make head and tail point to NULL
		delete nowNode; //DELETE LIST
	}
	else //If head and tail point at not same locate
	{
		while (nowNode->next != tail) //Searching the previous tail
		{
			nowNode = nowNode->next; //Moving the node poiter
		}
		delete atTail; //Delete old tail
		tail = nowNode; //Make new tail point at previous old tail
		tail->next = NULL; //Make tail linked to Null44
		return el; //Return element that old tail stored
	}
}
bool List::search(char el) //search character
{
	Node *check = head; //Make node point to head
	while (check->next != NULL) //Prevent while loop out of scope
	{
		if (check->data == el) //if element same as data that list store return true to booleen
		{
			return true;
		}
		else //if element not same as data that list store make check pointer moving to other list
		{
			check = check->next;
		}
	}
	return false; //if there is not same as element return false
}
void List::reverse() //reverse side list
{
		Node *nowNode = head; //Make nowNode point to head
		Node *prev = NULL; //Make nowNode point to NULL
		Node *nextNode = NULL; //Make nowNode point to NULL
		tail = head; //Make tail point at head because the reverse
		while (nowNode != NULL) //To finding the tail
		{
			nextNode = nowNode->next; //Move nextNode pointer to next list for make new list link to the old point in the future
			nowNode->next = prev; //to make list link previous
			prev = nowNode; //after linked already make pointer name previous point to the current nowNode point
			nowNode = nextNode; //make nowNode point to next list
		}
		head = prev; //Make head point to old tail pointing
}
void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}
