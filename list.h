#ifndef LIST
#define LIST

class Node {
public:
	char data;
	Node *next;
	Node(char el, Node *ptr = 0)
	{
		data = el; next = ptr;
	}
};

class List {
public:
	List() { head = tail = 0; } //Default pointer
	int isEmpty() { return head == 0; } //check empty list
	~List();
	void pushToHead(char el); //Add list to head
	void pushToTail(char el); //Add list to tail
	char popHead(); //delete the head
	char popTail(); //delete the tail
	bool search(char el); //seach the charactor in list
	void reverse(); //Reverse side list
	void print(); //Show list
private:
	Node * head, *tail;
};

#endif