#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	//Sample Code
	List mylist;
	
	//Add element to the list
	//add from head
	mylist.pushToHead('a');
	mylist.pushToHead('b');
	mylist.pushToHead('c');
	mylist.pushToHead('d');
	mylist.pushToHead('e');
	//add from tail
	mylist.pushToTail('f');
	mylist.pushToTail('g');
	mylist.pushToTail('h');
	mylist.pushToTail('i');
	mylist.pushToTail('j');
	
	//Show the original list
	cout << "Original list : "; mylist.print();
	cout << endl;

	//Show the Reverse list
	cout << "Reverse list : "; mylist.reverse(); mylist.print();
	cout << endl;
	
	//Told that isn't palindrome
	cout << "It's not a palindrome!" << endl;
	
	//popHead test , show element is deleted , show list
	cout << "popHead : " << mylist.popHead() << endl;
	cout << "List now : "; mylist.print();
	cout << endl;

	//poptail test , show element is deleted , show list
	cout << "popTail : "<< mylist.popTail() <<endl; 
	cout << "List now : "; mylist.print();
	cout << endl;

	//Seach test , make default search is Z if you need diffarent seach just change in mylist.seach('...') 
	cout << "Searhing : Z" << endl;
	if (mylist.search('Z') == true) //If there is.
		cout << "The Z is in the list.";
	else //If there isn't.
		cout << "The Z isn't in the list.";

	
	cout << endl;
	system("PAUSE");
	//TO DO! Write a program that tests your list library - the code should take characters, push them onto a list, 
	//- then reverse the list to see if it is a palindrome!

}